%include "lib.inc"

section .text

global find_word

; Ищет значение в словаре по ключу
; input:
;   rdi - адрес искомой строки (ключа)
;   rsi - адрес первого элемента словаря
; output:
;   rax - адрес найденной строки
find_word:
    ; для ориентирования по памяти в словаре
    NEXT_ELEMENT_OFFSET equ 0; первый элемент в памяти - указатель на следующее звено связного списка
    KEY_OFFSET equ 8; второй - адрес строки-ключа
    VALUE_OFFSET equ 16; третий - адрес строки-значения
    
    push r10
    push r11
    mov r10, rdi
    mov r11, rsi

    .loop:
        test r11, r11
        jz .not_found

        mov rdi, [r11 + KEY_OFFSET]
        mov rsi, r10
        call string_equals
        test rax, rax
        jnz .found

        mov r11, [r11 + NEXT_ELEMENT_OFFSET]
        jmp .loop
        
    .found:
        mov rax, r11
        add rax, VALUE_OFFSET
        pop r11
        pop r10
        ret
        
    .not_found:
        xor rax, rax
        pop r11
        pop r10
        ret

