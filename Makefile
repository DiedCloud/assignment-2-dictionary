SRC_DIR=src# SRC folder
OUT_DIR=build# build output folder
SRC=$(wildcard $(SRC_DIR)/*.asm)
OBJ=$(SRC:$(SRC_DIR)/%.asm=$(OUT_DIR)/%.o)# object files
EXE=app# result program name

all: $(SRC) $(EXE)

$(OUT_DIR)/dict.o: $(SRC_DIR)/lib.inc 
$(OUT_DIR)/main.o: $(SRC_DIR)/dict.inc $(SRC_DIR)/words.inc $(SRC_DIR)/lib.inc $(SRC_DIR)/colon.inc

#Create EXE
$(EXE): $(OBJ) 
	ld $(OBJ) -o $@

#Compile assembly program
$(OUT_DIR)/%.o: $(SRC_DIR)/%.asm
	@mkdir -p $(@D)
	nasm -felf64 -g -isrc $< -o $@

run: $(EXE)
	./$(EXE)
 
#Clean folder
clean:
	rm -rf $(OUT_DIR) $(EXE)

test: $(EXE)
	python3 tests.py

.PHONY: all clean run test
