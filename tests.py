#!/usr/bin/env python3
import unittest
import subprocess


class TestLab2(unittest.TestCase):
    def run_app(self, inp):
        pipe = subprocess.PIPE
        proc = subprocess.Popen(["./app"], text=True, shell=True, stdin=pipe, stdout=pipe, stderr=pipe)
        stdout, stderr = proc.communicate(input=inp)
        return stdout.strip(), stderr.strip(), proc.returncode

    def test_common(self):
        self.assertEqual(self.run_app("first word"), ("first word explanation", "", 0))
        self.assertEqual(self.run_app("second word"), ("second word explanation", "", 0))

    def test_wrong_key(self):
        self.assertEqual(self.run_app("xyz"), ("Not found", "", 1))

    def test_long_input(self):
        self.assertEqual(self.run_app("X" * 300), ("Read error", "", 1))


if __name__ == "__main__":
    unittest.main()
