%macro colon 2
    %ifid %2; если 2 параметр (метка) есть
        %%key: db %1, 0; ключ = первый параметр + null-терминатор
        %2:
            %ifdef first_dict_element; если уже есть >=1 элемента словаря, то нужно присоединить новый ко всему словарю
                dq first_dict_element
            %else
                dq 0; будет в последнем елементе.
            %endif
            dq %%key; указатель на ключ

        %define first_dict_element %2; переопределяем метку первого элемента словаря
    %else
        %error "colon: expected label"
    %endif
%endmacro

