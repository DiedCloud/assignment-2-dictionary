%include "lib.inc"
%include "words.inc"
%include "dict.inc"

%define BUF_SIZE 256

section .rodata
read_error_msg: db "Read error", 0
key_error_msg: db "Not found", 0

section .bss
buf: resb BUF_SIZE

section .text

global _start

_start:
    mov rdi, buf
    mov rsi, BUF_SIZE
    call read_word      ; was changed. Now ` ` doesn't stop reading
    test rax, rax
    jz .read_error

    mov rdi, buf 
    mov rsi, word_list  ; start address
    call find_word
    test rax, rax
    jz .key_error

    mov rdi, rax        ; print res from rax
    call print_string

    xor rdi, rdi
    call exit

    .read_error:
        mov rdi, read_error_msg
        jmp .finish

    .key_error:
        mov rdi, key_error_msg

    .finish:
        call print_string
        mov rdi, 1
        call exit
    

