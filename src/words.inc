%include "colon.inc"

section .rodata

colon "first word", first_word
db "first word explanation", 0 

colon "second word", second_word
db "second word explanation", 0 

colon "third word", third_word
db "third word explanation", 0

colon "very long name", very_long_name
db "very long name text", 0

word_list equ first_dict_element; определяем адрес словаря как адрес первого элемента связного списка
%undef first_dict_element; и избаляемся от лишних меток
